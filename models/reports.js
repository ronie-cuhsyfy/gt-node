var pg = require('pg');
// var conString = "postgres://cushyfy_dev:icanseeyou@syngenta.cfp6zcfiie2h.ap-southeast-1.rds.amazonaws.com:5432/syngenta_dev"; //dev
var conString = "postgres://cushyfy_dev:icanseeyou@172.20.10.6:5432/syngenta"; //dev
// var conString = "postgres://cushyfy_dev:icanseeyou@syngenta.cfp6zcfiie2h.ap-southeast-1.rds.amazonaws.com:5432/syngenta"; //prod
var db = require("../dbcon"); //reference of dbconnection.js
var client = new pg.Client(conString);
client.connect();

var reports = {

    addReceipt: function (info, callback) {
        // console.log("insert into receipt (membership_id,link,user_type,created) values ('" + info.membership_id + "','" + info.link + "','" + info.user_type + "','" + new Date().toISOString()+ "')")
        return db.query("insert into receipt (membership_id,link,user_type,created,token,invoice,retailerId) values ('" + info.membership_id + "','" + info.link + "','" + info.user_type + "','" + new Date().toISOString()+ "','"+info.token+"','"+info.invoice+"','"+info.retailer_id+"')", callback);
    },
    freeQuery: function (strQuery, callback) {
        return client.query(strQuery, callback);
    },
    freeQueryDB: function (strQuery, callback) {
        return db.query(strQuery, callback);
    },
    updateStat: function (id, callback) {
        return db.query("update transdetails set status = 1 where transid = " + id, callback);
    },
    getFFMsg: callback => {
        return client.query("select * from messages_fieldforcemessage ", callback);
    },
    geRetQTYByFF: function (id, callback) {
        return client.query("select * from rewards_producttxnitem as item " +
            "left join rewards_producttxn as prod on prod.id = item.product_txn_id " +
            "left join rewards_txn as txn on txn.id = prod.txn_id  " +
            "left join rewards_product as pd on pd.id = item.product_id " +
            "where  txn.content_type_id = (select id from django_content_type where model = 'retailer') and txn.status = 1 and txn.object_id in (select id from users_retailer where fieldforce_id = " + id + " ) ", callback);
    },

    getRetTotalPoints: callback => {
        return client.query("select sum(points) from rewards_txn where content_type_id = (select id from django_content_type where model = 'retailer') and status = 1 and type = 'claim'", callback);
    },
    getGroTotalPoints: callback => {
        return client.query("select sum(points) from rewards_txn where content_type_id = (select id from django_content_type where model = 'grower') and status = 1", callback);
    },
    getTopFFs: callback => {
        return client.query("select ff.*,coalesce(sum(txn.points), 0) as points, 0 as amt from users_fieldforce as ff " +
            "left join users_retailer as ret  on ret.fieldforce_id = ff.id " +
            "left join rewards_txn as txn on txn.object_id = ret.id where txn.content_type_id = (select id from django_content_type where model = 'retailer') or txn.content_type_id = (select id from django_content_type where model = 'fieldforce') and txn.status = 1 and txn.type = 'claim' " +
            "group by ff.id order by points desc", callback)
    },
    getTopRet: callback => {
        return client.query("select ret.*,coalesce(sum(txn.points), 0) as points, 0 as amt from users_retailer as ret " +
            "left join rewards_txn as txn on txn.object_id = ret.id where txn.content_type_id = (select id from django_content_type where model = 'retailer') and txn.status = 1 and txn.type = 'claim' " +
            "group by ret.id order by points desc ", callback);
    },
    getTopRetByFF: function (id, callback) {
        return client.query("select ret.*,coalesce(sum(txn.points), 0) as points, 0 as amt from users_retailer as ret " +
            "left join rewards_txn as txn on txn.object_id = ret.id where txn.content_type_id = (select id from django_content_type where model = 'retailer') and txn.status = 1 and txn.type = 'claim' and ret.fieldforce_id = " + id +
            " group by ret.id order by points desc ", callback);
    },

    getTopGro: callback => {
        return client.query("select ret.*,coalesce(sum(txn.points), 0) as points, 0 as amt from users_grower as ret " +
            "left join rewards_txn as txn on txn.object_id = ret.id where txn.content_type_id = (select id from django_content_type where model = 'grower') and txn.status = 1 and txn.type = 'claim' " +
            "group by ret.id order by points desc", callback);
    },
    getRetTransByFF: function (id, callback) {
        console.log(id);
        return db.query("select * from transdetails  where type = 'retailer' and status = 1 and fieldforce = " + id, callback);
    },
    getGroTransByFF: function (id, callback) {
        console.log(id);
        return db.query("select * from transdetails  where type = 'grower' and status = 1 and fieldforce = " + id, callback);
    },
    getRetTrans: callback => {
        return db.query("select * from transdetails  where type = 'retailer' and status = 1 ", callback)
    },
    getTrans: callback => {
        return db.query("select * from transdetails  where status = 1 ", callback)
    },
    getGroTrans: callback => {
        return db.query("select * from transdetails  where type = 'grower' and status = 1 ", callback)
    },
    getRetTotalAMT: callback => {
        return db.query("select sum(total_amt) as total from transdetails  where type = 'retailer' and status = 1 ", callback)
    },
    getRetTotalAMT3: callback => {
        return db.query("select * from transdetails  where type = 'retailer' and status = 3 ", callback)
    },
    getGroTotalAMT: callback => {
        return db.query("select sum(total_amt) as total from transdetails  where type = 'grower' and status = 1 ", callback)
    },
    getTopFFAMT: callback => {
        return db.query("select sum(total_amt) as total,fieldforce from transdetails  where type = 'retailer' and status = 1 group by fieldforce  order by total desc", callback)
    },
    getTopRetAMT: callback => {
        return db.query("select sum(total_amt) as total,userid from transdetails  where type = 'retailer' and status = 1 group by userid  order by total desc", callback)
    },
    getTopGroAMT: callback => {
        return db.query("select sum(total_amt) as total,userid from transdetails  where type = 'grower' and status = 1 group by userid  order by total desc", callback)
    },
    getAMT: function (type, callback) {

    },
    geRetTotal: function (id, callback) {
        return db.query("select * from transdetails  where type = 'retailer' and status = 1 and userid = " + id, callback);
    },
    getRetTotalAMTByID: function (id, callback) {
        return db.query("select (select sum(total_amt) as total from transdetails  where type = 'retailer' and status = 1 and userid = " + id + ") as total,trans.* from transdetails as trans  where trans.type = 'retailer' and trans.status = 1 and trans.userid = " + id, callback);
    },
    getRetTotalAMTByIDFF: function (id, callback) {
        return db.query("select (select sum(total_amt) as total from transdetails  where type = 'retailer' and status = 1 and userid = trans.userid) as total,trans.* from transdetails as trans  where trans.type = 'retailer' and trans.status = 1 and trans.fieldforce = " + id, callback);
    },
    getGroTotalAMTByIDFF: function (id, callback) {
        return db.query("select (select sum(total_amt) as total from transdetails  where type = 'grower' and status = 1 and userid = trans.userid) as total,trans.* from transdetails as trans  where trans.type = 'grower' and trans.status = 1 and trans.fieldforce = " + id, callback);
    },
    getGroTotalAMTByID: function (id, callback) {
        return db.query("select (select sum(total_amt) as total from transdetails  where type = 'grower' and status = 1 and userid = " + id + ") as total,trans.* from transdetails as trans  where trans.type = 'grower' and trans.status = 1 and trans.userid = " + id, callback);
    },
    geRetQTY: function (id, callback) {
        return client.query("select * from rewards_producttxnitem as item " +
            "left join rewards_producttxn as prod on prod.id = item.product_txn_id " +
            "left join rewards_txn as txn on txn.id = prod.txn_id  " +
            "left join rewards_product as pd on pd.id = item.product_id " +
            "where  txn.content_type_id = (select id from django_content_type where model = 'retailer') and txn.status = 1 and txn.object_id =  " + id, callback);
    },
    geGroQTY: function (id, callback) {
        return client.query("select * from rewards_producttxnitem as item " +
            "left join rewards_producttxn as prod on prod.id = item.product_txn_id " +
            "left join rewards_txn as txn on txn.id = prod.txn_id  " +
            "left join rewards_product as pd on pd.id = item.product_id " +
            "where  txn.content_type_id = (select id from django_content_type where model = 'grower') and txn.status = 1 and txn.object_id =  " + id, callback);
    },
    getFFMC: function (id, callback) {
        return client.query("select " +
            "COALESCE((select count(retf.id) from users_grower as retf where retf.content_type_id = (select id from django_content_type where model = 'fieldforce') and retf.object_id = ff.id))+ " +
            "COALESCE((select count(retg.id) from users_grower as retg where retg.content_type_id = (select id from django_content_type where model = 'retailer') and retg.object_id in(select ret.id from users_retailer as ret where ret.fieldforce_id = ff.id) )) as gc ," +
            "(select count(id) from users_retailer where fieldforce_id = ff.id ) as rc, " +
            "COALESCE((select count(retf.id) from users_grower as retf where retf.content_type_id = (select id from django_content_type where model = 'fieldforce') and retf.object_id = ff.id))+ " +
            "COALESCE((select count(retg.id) from users_grower as retg where retg.content_type_id = (select id from django_content_type where model = 'retailer') and retg.object_id in(select ret.id from users_retailer as ret where ret.fieldforce_id = ff.id) ))+ " +
            "COALESCE ((select count(id) from users_retailer where fieldforce_id = ff.id )) as tc, " +
            "ff.*,au.email " +
            "from users_fieldforce as ff left join auth_user as au on au.id = ff.user_id where ff.id = " + id, callback);
    },
    getTopFF: callback => {
        return client.query("select " +
            "COALESCE((select count(retf.id) from users_grower as retf where retf.content_type_id = (select id from django_content_type where model = 'fieldforce') and retf.object_id = ff.id))+ " +
            "COALESCE((select count(retg.id) from users_grower as retg where retg.content_type_id = (select id from django_content_type where model = 'retailer') and retg.object_id in(select ret.id from users_retailer as ret where ret.fieldforce_id = ff.id) )) as gc ," +
            "(select count(id) from users_retailer where fieldforce_id = ff.id ) as rc, " +
            "COALESCE((select count(retf.id) from users_grower as retf where retf.content_type_id = (select id from django_content_type where model = 'fieldforce') and retf.object_id = ff.id))+ " +
            "COALESCE((select count(retg.id) from users_grower as retg where retg.content_type_id = (select id from django_content_type where model = 'retailer') and retg.object_id in(select ret.id from users_retailer as ret where ret.fieldforce_id = ff.id) ))+ " +
            "COALESCE ((select count(id) from users_retailer where fieldforce_id = ff.id )) as tc, " +
            "ff.*" +
            "from users_fieldforce as ff order by tc desc", callback);
    },
    getTopFFByGrower: callback => {
        return client.query("select " +
            "COALESCE((select count(retf.id) from users_grower as retf where retf.content_type_id = (select id from django_content_type where model = 'fieldforce') and retf.object_id = ff.id))+ " +
            "COALESCE((select count(retg.id) from users_grower as retg where retg.content_type_id = (select id from django_content_type where model = 'retailer') and retg.object_id in(select ret.id from users_retailer as ret where ret.fieldforce_id = ff.id) )) as gc ," +
            "ff.*" +
            "from users_fieldforce as ff order by gc desc limit 5", callback);
    },
    getRetailerBalance: function (id, callback) {
        return client.query("select * from rewards_points where content_type_id = (select id from django_content_type where model = 'retailer') and object_id = " + id, callback);
    },
    getGrowererBalance: function (id, callback) {
        return client.query("select * from rewards_points where content_type_id = (select id from django_content_type where model = 'grower') and object_id = " + id, callback);
    },
    getAllAccount: callback => {
        return client.query("Select * from users_fieldforce", callback);
    },
    getRetailerTransItem: function (id, callback) {
        return client.query("select items.id as prodid,items.quantity,prod.name,points.points,prod.*  from rewards_producttxnitem as items "
            + "left join rewards_product as prod on prod.id = items.product_id "
            + "left join rewards_productpoints as points on points.product_id = items.product_id and points.user_type = 'retailer' and points.tier = 1 "
            + "where items.product_txn_id = (select id from rewards_producttxn where txn_id = " + id + " ) ", callback);
    },
    getGrowerTransItem: function (id, callback) {
        return client.query("select items.id as prodid,items.quantity,prod.name,points.points,prod.*  from rewards_producttxnitem as items "
            + "left join rewards_product as prod on prod.id = items.product_id "
            + "left join rewards_productpoints as points on points.product_id = items.product_id and points.user_type = 'grower' and points.tier = 1 "
            + "where items.product_txn_id = (select id from rewards_producttxn where txn_id = " + id + " ) ", callback);
    },
    getTransItemAmount: function (id, callback) {
        return db.query("select * from transdetails where transid = " + id, callback);
    },
    getGrowerDenied: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate,trans.status as status,trans.modified as modifieddate,ptxn.receipt_photo, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') and trans.status = 4 "
            + "order by submitteddate desc", callback);
    },
    getGrowerApproved: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate,trans.status as status,trans.modified as modifieddate,ptxn.receipt_photo, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') and trans.status = 1 "
            + "order by submitteddate desc", callback);
    },
    getGrowerPending: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate,trans.status as status,trans.modified as modifieddate,ptxn.receipt_photo, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') and trans.status = 2 "
            + "order by submitteddate desc", callback);
    },
    getRetailerDenied: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate, "
            + "trans.modified as modifieddate,  CONCAT(ff.first_name,' ',ff.middle_name,' ',ff.last_name) as fieldforce,"
            + "ptxn.receipt_photo,trans.status as status, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "left join users_fieldforce as ff on ff.id = ret.fieldforce_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 4 "
            + "order by submitteddate desc", callback);
    },
    getRetailerAccepted: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate, "
            + "trans.modified as modifieddate,  CONCAT(ff.first_name,' ',ff.middle_name,' ',ff.last_name) as fieldforce,"
            + "ptxn.receipt_photo,trans.status as status, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "left join users_fieldforce as ff on ff.id = ret.fieldforce_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 1 "
            + "order by submitteddate desc", callback);
    },
    getRetailerApproved: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate, "
            + "trans.modified as modifieddate,  CONCAT(ff.first_name,' ',ff.middle_name,' ',ff.last_name) as fieldforce,"
            + "ptxn.receipt_photo,trans.status as status, "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "left join users_fieldforce as ff on ff.id = ret.fieldforce_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 3 "
            + "order by submitteddate desc", callback);
    },
    getRetailerPending: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name,ret.id as userid, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate, "
            + "trans.modified as modifieddate,  CONCAT(ff.first_name,' ',ff.middle_name,' ',ff.last_name) as fieldforce,"
            + "ptxn.receipt_photo,trans.status as status , "
            + " ptxn.receipt_from "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "left join users_fieldforce as ff on ff.id = ret.fieldforce_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 2 "
            + "order by submitteddate desc", callback);
    },
    getRecentAccepted: callback => {
        return client.query("select trans.id as transid,ret.membership_id as membershipid, "
            + "CONCAT(ret.first_name,' ',ret.middle_name,' ',ret.last_name) as name, "
            + "ptxn.receipt_number as invoice,trans.created as submitteddate "
            + "from rewards_producttxn as ptxn "
            + "left join rewards_txn as trans on trans.id = ptxn.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 1 "
            + "order by submitteddate desc limit 10", callback);
    },
    getRetailerByID: function (id, callback) {
        return client.query("  select *,(select sum(points) from rewards_txn where content_type_id = (select id from django_content_type where model = 'retailer') and type = 'redeem' and status = 1 and object_id = " + id + " ) as sum from users_retailer where id = " + id, callback);
    },
    getGroByID: function (id, callback) {
        return client.query("  select *,(select sum(points) from rewards_txn where content_type_id = (select id from django_content_type where model = 'grower') and type = 'redeem' and status = 1 and object_id = " + id + " ) as sum from users_grower where id =  " + id, callback);
    },
    getRetailerRefferal: function (id, callback) {
        return client.query("  select * from users_retailer where referrer_id =  " + id, callback);
    },
    getRetailerByFielForce: function (id, callback) {
        return client.query("select * from users_retailer where fieldforce_id = " + id + " order by id desc", callback);
    },
    getGroByFielForce: function (id, callback) {
        return client.query("select * from users_grower where content_type_id = (select id from django_content_type where model = 'retailer') and object_id in (select id from users_retailer where fieldforce_id = " + id + ")" +
            "or content_type_id = (select id from django_content_type where model = 'fieldforce') and object_id = " + id + " order by id desc", callback);
    },
    getGrowerByFielForce: function (id, callback) {
        return client.query("select * from users_grower order by id desc", callback);
    },
    getTopByReilertaFieldForceByID: function (id, callback) {
        console.log("select *,(select count(id) from users_retailer where fieldforce_id = ff.id ) as ct "
            + "from users_fieldforce as ff  "
            + "where ff.id = " + id
            + " order by ct desc");
        return client.query("select *,(select count(id) from users_retailer where fieldforce_id = ff.id ) as ct "
            + "from users_fieldforce as ff  "
            + "where ff.id = " + id
            + " order by ct desc", callback);
    },
    getFieldForceByID: function (id, callback) {
        return client.query("select * from users_fieldforce where id= " + id, callback);
    },
    getRetailerTransCount: callback => {
        return client.query("select count(prod.id) from rewards_producttxn as prod left join rewards_txn as trans on trans.id = prod.txn_id where trans.content_type_id = (select id from django_content_type where model = 'retailer')", callback);
    },
    getGrowerTransCount: callback => {
        return client.query("select count(prod.id) from rewards_producttxn as prod left join rewards_txn as trans on trans.id = prod.txn_id where trans.content_type_id = (select id from django_content_type where model = 'grower')", callback);
    },
    getRetailerRedeemCount: callback => {
        return client.query("select count(prod.id) from rewards_redeemtxn as prod left join rewards_txn as trans on trans.id = prod.txn_id where trans.content_type_id = (select id from django_content_type where model = 'retailer')", callback);
    },
    getGrowerRedeemCount: callback => {
        return client.query("select count(prod.id) from rewards_redeemtxn as prod left join rewards_txn as trans on trans.id = prod.txn_id where trans.content_type_id = (select id from django_content_type where model = 'grower')", callback);
    },
    getTopByReilertaFieldForce: callback => {
        return client.query("select *,(select count(id) from users_retailer where fieldforce_id = ff.id ) as ct "
            + "from users_fieldforce as ff  "
            + "order by ct desc limit 5 ", callback);
    },
    getFieldForce: callback => {
        return client.query("select uf.*,au.email from users_fieldforce as uf left join auth_user as au on au.id = uf.user_id order by uf.id desc", callback);
    },
    getRetailerRecent: callback => {
        return client.query("select * from users_retailer order by id desc limit 5", callback);
    },
    getGrowerRecent: callback => {
        return client.query("select * from users_grower order by id desc limit 5", callback);
    },
    getRetailer: callback => {
        return client.query("select * from users_retailer order by id desc", callback);
    },
    getGrowerByID: function (id, callback) {
        return client.query("select * from users_grower where id= " + id, callback);
    },
    getGrower: callback => {
        return client.query("select * from users_grower order by id desc", callback);
    },
    getRecentRetailer: callback => {
        return client.query("select * from users_retailer order by id desc limit 5", callback);
    },
    getRecentGrower: callback => {
        return client.query("select * from users_grower order by id desc limit 5", callback);
    },
    getRecentRedemptionRetailer: callback => {
        return client.query("select prod.*,ret.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') "
            + "order by prod.id desc limit 5", callback);
    },
    getRecentRedemptionGrower: callback => {
        return client.query("select prod.*,ret.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') "
            + "order by prod.id desc limit 5", callback);
    },
    getTopRetailerGross: callback => {
        return client.query("select r.*,"
            + "(select count(prod.id) from rewards_producttxn as prod "
            + " left join rewards_txn as trans on trans.id = prod.txn_id "
            + " where content_type_id =(select id from django_content_type where model = 'retailer') and object_id=r.id "
            + ")as tc "
            + "from users_retailer as r order by tc desc limit 5 ", callback);
    },
    getTopRetailerGrossByFF: function (id, callback) {
        return client.query("select r.*,"
            + "(select count(prod.id) from rewards_producttxn as prod "
            + " left join rewards_txn as trans on trans.id = prod.txn_id "
            + " where content_type_id =(select id from django_content_type where model = 'retailer') and object_id=r.id "
            + ")as tc "
            + "from users_retailer as r where r.fieldforce_id = " + id + " order by tc desc limit 1 ", callback);
    },
    getTopGrowerGross: callback => {
        return client.query("select r.*,"
            + "(select count(prod.id) from rewards_producttxn as prod "
            + " left join rewards_txn as trans on trans.id = prod.txn_id "
            + " where content_type_id =(select id from django_content_type where model = 'grower') and object_id=r.id "
            + ")as tc "
            + "from users_grower as r order by tc desc limit 5 ", callback);
    },
    getTopCrop: callback => {
        return client.query("select *,(select count(id) from rewards_producttxnitem where product_id = prod.id ) as ct "
            + "from rewards_product as prod "
            + "where prod.business_unit = 'crop_protection' "
            + "order by ct desc limit 5", callback);
    },
    getTopSeed: callback => {
        return client.query("select *,(select count(id) from rewards_producttxnitem where product_id = prod.id ) as ct "
            + "from rewards_product as prod "
            + "where prod.business_unit <> 'crop_protection' "
            + "order by ct desc limit 5", callback);
    },
    getRecentRetailerTrans: callback => {
        return client.query("select prod.*,ret.* from rewards_producttxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 1 "
            + "order by prod.id desc limit 5", callback)
    },
    getRecentGrowerTrans: callback => {
        return client.query("select prod.*,ret.* from rewards_producttxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') and trans.status = 1 "
            + "order by prod.id desc limit 5", callback)
    },
    getRecentGrowerRedeem: callback => {
        return client.query("select prod.*,ret.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') "
            + "order by prod.id desc limit 5", callback)
    },
    getRecentRetailerRedeem: callback => {
        return client.query("select prod.*,ret.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') "
            + "order by prod.id desc limit 5", callback)
    },
    getRetailerRedeem: function (id, callback) {
        return client.query("select prod.*,ret.*,rew.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_retailer as ret on ret.id = trans.object_id "
            + "left join rewards_reward as rew on rew.id = prod.reward_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'retailer') and trans.status = 1 and trans.object_id = " + id
            + "order by prod.id desc", callback);
    },
    getGrowerRedeem: function (id, callback) {
        return client.query("select prod.*,ret.*,rew.* from rewards_redeemtxn as prod "
            + "left join rewards_txn as trans on trans.id = prod.txn_id "
            + "left join users_grower as ret on ret.id = trans.object_id "
            + "left join rewards_reward as rew on rew.id = prod.reward_id "
            + "where trans.content_type_id = (select id from django_content_type where model = 'grower') and trans.object_id = " + id
            + "order by prod.id desc", callback);
    },

}
module.exports = reports; ``