var express = require('express');
var router = express.Router();
const https = require('https');
var request = require('request');
var report = require('../models/reports');
const AWS = require('aws-sdk');
// var host = "http://54.169.232.8:8004";
// var host = "https://api.syngentatiwala.com";
var host = "https://dev-inventory.syngentatiwala.com";


/* GET home page. */

router.post('/claim/receipt/', (req, res, next) => {
  console.log(req.body);
  report.addReceipt(req.body, (err, resuslt) => {

    if (err) {
      res.json({ result: "Error", details: err });
    }
    else {
      res.json({ result: "Success", details: resuslt });
    }
  });
});

router.post('/barcodes/barcode/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(req);
    let data = req.body.data;
    //   let token = "Token " + req.body.token;
    let token = "Token " + body.auth_token;
    request(
      {
        url: host + "/barcodes/barcode/",
        method: "POST",
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        },
        body: data
      },
      function (error, response, body) {
        res.json(body);
        console.log(body);
      }
    );
  });
});
router.post('/call/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(req);
    let data = req.body.data;
    //   let token = "Token " + req.body.token;
    let token = "Token " + body.auth_token;
    request(
      {
        url: host + data.url,
        method: "POST",
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        },
        body: data.body
      },
      function (error, response, body) {
        res.json(body);
        console.log(body);
      }
    );
  });
});
router.post('/barcodes/get/barcode/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(req);
    let data = req.body.data;
    //   let token = "Token " + req.body.token;
    let token = "Token " + body.auth_token;
    request(
      {
        url: host + data.url,
        method: "GET",
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        },
        body: data
      },
      function (error, response, body) {
        console.log(host + data.url);
        res.json(body);

      }
    );
  });
});

router.post('/users/devices/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/devices/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(response);
    console.log(response);
  });
});

router.get("/rewards/giftaway/redemptions/:id", (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(req);
    let data = { status: 0 };
    //   let token = "Token " + req.body.token;
    let token = "Token " + body.auth_token;
    request(
      {
        url: host + "/rewards/giftaway/redemptions/" + req.params.id,
        method: "PATCH",
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        },
        body: data
      },
      function (error, response, body) {
        res.json(body);
        console.log(body);
      }
    );
  });
});
router.get("/rewards/sharetreats/redemptions/:id", (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(req);
    let data = { status: 0 };
    //   let token = "Token " + req.body.token;
    let token = "Token " + body.auth_token;
    request(
      {
        url: host + "/rewards/sharetreats/redemptions/" + req.params.id,
        method: "PATCH",
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        },
        body: data
      },
      function (error, response, body) {
        res.json(body);
        console.log(body);
      }
    );
  });
});
router.get('/rewards/sharetreats/redemptions/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/rewards/sharetreats/redemptions/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/rewards/giftaway/redemptions/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/rewards/giftaway/redemptions/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/get/rewards/giftaway/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/rewards/giftaway/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.post('/rewards/redeem/validate/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/rewards/redeem/validate/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});

router.post('/users/retailers/refer/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/refer/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/rewards/growers/redeem/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  console.log("body", data);
  console.log("Authorization", token);
  request({
    url: host + "/rewards/growers/redeem/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/rewards/retailers/redeem/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/rewards/retailers/redeem/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/retailers/me/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/users/retailers/me/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
// 
// router.get('/rewards/groer/redeem/:id?', (req, res, next) => {
//   let token = 'Token ' + req.params.id;
//   // console.log(token);
//   request({
//     url: host+"/rewards/growers/redeem/",
//     method: "GET",
//     json: true,
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': token
//     },
//   }, function (error, response, body) {
//     res.json(body);
//     console.log(body);
//   });
// });
router.get('/rewards/giftaways/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/rewards/sharetreats/redemptions/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/rewards/giftaway/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  console.log(token);
  request({
    url: host + "/rewards/giftaway/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    // console.log(body);
  });
});

router.get('/rewards/loadcentral/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  console.log(token);
  request({
    url: host + "/rewards/loadcentral/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    // console.log(body);
  });
});

router.get('/rewards/sharetreats/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/rewards/sharetreats/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    // console.log(body);
  });
});

router.post('/upload/img/', (req, res, next) => {
  const BUCKET_NAME = 'syngentaapp';
  const IAM_USER_KEY = 'AKIAIUEC2WQ5Q52TYFBA';
  const IAM_USER_SECRET = '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';
  let file = req.body.file;
  console.log(req);
  let s3bucket = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: BUCKET_NAME,
  });
  s3bucket.createBucket(function () {
    var params = {
      Bucket: BUCKET_NAME,
      Key: file.name,
      Body: file,
    };
    s3bucket.upload(params, function (err, data) {
      if (err) {
        console.log('error in callback');
        console.log(err);
      }
      console.log('success');

      console.log(params);
      console.log(err);
      res.json(data);


    });
  });
});
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/get/fieldforce/msg/', (req, res, next) => {
  report.getFFMsg((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/sample/:id?/:member?', (req, res, next) => {
  console.log(req.params.id);
  console.log(req.params.member);
});
router.post('/get/query/', (req, res, next) => {
  console.log(req.body.query);
  report.freeQuery(req.body.query, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.post('/get/query/db/', (req, res, next) => {
  console.log(req.body.query);
  report.freeQueryDB(req.body.query, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/validation/approve/:id?', (req, res, next) => {
  report.updateStat(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/fieldforce/member/count/:id?', (req, res, next) => {
  report.getFFMC(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/retailer/gross/by/id/:id?', (req, res, next) => {
  report.getRetTotalAMTByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/fieldforce/retailer/gross/by/id/:id?', (req, res, next) => {
  report.getRetTotalAMTByIDFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/fieldforce/grower/gross/by/id/:id?', (req, res, next) => {
  report.getGroTotalAMTByIDFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/grower/gross/by/id/:id?', (req, res, next) => {
  report.getGroTotalAMTByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/retailer/points/balance/:id?', (req, res, next) => {
  report.getRetailerBalance(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/grower/points/balance/:id?', (req, res, next) => {
  report.getGrowererBalance(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/fieldforce/top/retailer/:id?', (req, res, next) => {
  report.getTopRetailerGrossByFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/grower/trans/items/:id?', (req, res, next) => {
  report.getGrowerTransItem(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/trans/items/:id?', (req, res, next) => {
  report.getRetailerTransItem(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/fieldforce/grower/all/trans/:id?', (req, res, next) => {
  console.log(req.params.id);
  report.getGroTransByFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/fieldforce/retailer/all/trans/:id?', (req, res, next) => {
  console.log(req.params.id);
  report.getRetTransByFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/retailer/all/trans', (req, res, next) => {
  report.getRetTrans((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/all/trans', (req, res, next) => {
  report.getTrans((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/grower/all/trans', (req, res, next) => {
  report.getGroTrans((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/retailer/total/gross/pending', (req, res, next) => {
  report.getRetTotalAMT3((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/retailer/total/gross/', (req, res, next) => {
  report.getRetTotalAMT((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/grower/total/gross/', (req, res, next) => {
  report.getGroTotalAMT((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/top/fieldforce/', (req, res, next) => {
  report.getTopFF((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/fieldforce/by/grower', (req, res, next) => {
  report.getTopFFByGrower((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/claim/1', (req, res, next) => {
  report.getGrowerApproved((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/claim/4', (req, res, next) => {
  report.getGrowerDenied((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/claim/2', (req, res, next) => {
  report.getGrowerPending((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/claim/1', (req, res, next) => {
  report.getRetailerAccepted((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/claim/3', (req, res, next) => {
  report.getRetailerApproved((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/claim/4', (req, res, next) => {
  report.getRetailerDenied((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/claim/2', (req, res, next) => {
  report.getRetailerPending((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/recent/claim/', (req, res, next) => {
  report.getRecentAccepted((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/fieldforce/retailer/:id?', (req, res, next) => {
  report.getRetailerByFielForce(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/fieldforce/grower/:id?', (req, res, next) => {
  report.getGroByFielForce(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailers/:id?', (req, res, next) => {
  report.getRetailerByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/growers/:id?', (req, res, next) => {
  report.getGroByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/retailer/refferal/:id?', (req, res, next) => {
  report.getRetailerRefferal(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/fieldforce/retailercount/:id?', (req, res, next) => {
  report.getTopByReilertaFieldForceByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/fieldforces/:id?', (req, res, next) => {
  report.getFieldForceByID(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/retailer/trans/count/', (req, res, next) => {
  report.getRetailerTransCount((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/redeem/count/', (req, res, next) => {
  report.getRetailerRedeemCount((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/trans/count/', (req, res, next) => {
  report.getGrowerTransCount((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/redeem/count/', (req, res, next) => {
  report.getGrowerRedeemCount((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/top/fieldforce/by/retailer/', (req, res, next) => {
  report.getTopByReilertaFieldForce((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/recent/grower/redeem/', (req, res, next) => {
  report.getRecentGrowerRedeem((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});


router.get('/get/recent/retailer/redeem/', (req, res, next) => {
  report.getRecentRetailerRedeem((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/retailer/redeem/:id?', (req, res, next) => {
  report.getRetailerRedeem(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/redeem/:id?', (req, res, next) => {
  report.getGrowerRedeem(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/recent/grower/trans/', (req, res, next) => {
  report.getRecentGrowerTrans((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});


router.get('/get/recent/retailer/trans/', (req, res, next) => {
  report.getRecentRetailerTrans((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/recent/retailer/', (req, res, next) => {
  report.getRetailerRecent((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/recent/grower/', (req, res, next) => {
  report.getGrowerRecent((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/get/fieldforce/', (req, res, next) => {
  report.getFieldForce((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/', (req, res, next) => {
  report.getRetailer((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/fieldforce/retailer/sales/qty/:id?', (req, res, next) => {
  report.geRetQTYByFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/sales/qty/:id?', (req, res, next) => {
  report.geRetQTY(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/sales/qty/:id?', (req, res, next) => {
  report.geGroQTY(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/', (req, res, next) => {
  report.getGrower((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/redemption/', (req, res, next) => {
  report.getRecentRedemptionRetailer((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/grower/redemption/', (req, res, next) => {
  report.getRecentRedemptionGrower((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/retailer/total/points/', (req, res, next) => {
  report.getRetTotalPoints((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/grower/total/points', (req, res, next) => {
  report.getGroTotalPoints((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows[0]);
    }
  });
});
router.get('/get/top/fieldforce/gross/', (req, res, next) => {
  report.getTopFFs((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/retailer/by/fieldforce/:id?', (req, res, next) => {
  report.getTopRetByFF(req.params.id, (err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/retailer/gross/', (req, res, next) => {
  report.getTopRet((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/fieldforce/gross/amt/', (req, res, next) => {
  report.getTopFFAMT((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/top/retailer/gross/amt/', (req, res, next) => {
  report.getTopRetAMT((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/top/grower/gross/amt/', (req, res, next) => {
  report.getTopGroAMT((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt);
    }
  });
});
router.get('/get/top/grower/gross/', (req, res, next) => {
  report.getTopGro((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/crop/', (req, res, next) => {
  report.getTopCrop((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});
router.get('/get/top/seed/', (req, res, next) => {
  report.getTopSeed((err, resuslt) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(resuslt.rows);
    }
  });
});

router.get('/rewards/retailers/claim/', (req, res, next) => {
  let token = 'Token 9929901ab0f92969b3b124a15b052c319b81b9c9';
  request({
    url: host + "/rewards/retailers/claim/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);

  });
});
router.post('/rewards/retailers/claim/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/rewards/retailers/claim/" + data.membership + "/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(response);
    console.log(response);
  });
});
router.post('/rewards/growers/claim/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  console.log(data);

  request({
    url: host + "/rewards/growers/claim/" + data.membership + "/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(response);
    console.log(error);
  });
});
//loadcentral
router.post('/rewards/loadcentral/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body.data;
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/loadcentral/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/get/rewards/sharetreats/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/sharetreats/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/rewards/loadcentrals/', (req, res, next) => {
  console.log("sdadsadasdas");
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/loadcentral/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
//giftaway
router.post('/rewards/sharetreats/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body.data;
    data.sharetreats.item_name = data.name
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/sharetreats/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.post('/rewards/giftaway/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body.data;
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/giftaway/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/get/rewards/giftaways/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    console.log(host + "/auth/token/login/");
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/giftaway/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
//products
router.post('/rewards/products/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body.data;
    let token = 'Token ' + body.auth_token;
    request({
      url: host + "/rewards/products/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });

});

router.post('/users/retailers/lostcard/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/lostcard/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});

router.post('/users/retailers/lostphone/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/lostphone/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});


router.post('/users/growers/lostcard/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/growers/lostcard/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});

router.post('/users/growers/lostphone/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/growers/lostphone/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});

router.get('/get/products/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/rewards/products/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/retailers/:id?/:member?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/users/retailers/?membership=" + req.params.member,
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/retailers/phone/:id?/:phone?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  console.log(host + "/users/retailers/?phone_number=" + req.params.phone);
  request({
    url: host + "/users/retailers/?phone_number=" + req.params.phone,
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/growers/phone/:id?/:phone?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/users/growers/?phone_number=" + req.params.phone,
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/growers/:id?/:member?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  // console.log(token);
  request({
    url: host + "/users/growers/?membership=" + req.params.member,
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
//fieldforce
router.post('/users/fieldforces/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body.data;
    let token = 'Token ' + body.auth_token;;
    request({
      url: host + "/users/fieldforces/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
//login
router.post('/auth/token/login/', (req, res, next) => {
  let data = req.body;
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
//retailers
router.get('/users/retailer/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  request({
    url: host + "/users/retailers/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.get('/users/fieldforces/me/:id?', (req, res, next) => {
  let token = 'Token ' + req.params.id;
  request({
    url: host + "/users/fieldforces/me/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/retailers/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/retailers/activate/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/activate/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/retailers/activate/resend/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/retailers/activate/resend/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
//growes
router.get('/users/growers/', (req, res, next) => {
  let token = 'Token eb2d28d8e1af71d05685126996c631cc09488ffd';
  request({
    url: host + "/users/growers/",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/growers/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/growers/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/growers/activate/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/growers/activate/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
router.post('/users/growers/activate/resend/', (req, res, next) => {
  let data = req.body.data;
  let token = 'Token ' + req.body.token;
  request({
    url: host + "/users/growers/activate/resend/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json',
    },
    body: data
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});
// http://54.169.232.8:8004/users/fieldforces/me/

router.get('/getcall/:url?/:token?', (req, res, next) => {
  let url = req.params.url.replace(/-/g, "/");
  let token = 'Token ' + req.params.token;
  console.log(url);
  request({
    url: url,
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });
});

router.post('/postcall', (req, res, next) => {
  let data = req.body;
  res.json(data)
});

router.post('/sms', (req, res, next) => {
  console.log(req.body.message);
  console.log(req.body.mobile);

  var url = "https://rest-beta.promotexter.com/sms/send";
  var apiKey = "90c221730c23df722c25d3de517a5ec0";
  var apiSecret = "d09de95920e858588a5bba3e13fdb493";
  var senderId = "SYNGENTA PH";

  var params = '?apiKey=' + apiKey + '&apiSecret=' + apiSecret + '&from=' +
    encodeURIComponent(senderId) + '&to=' + req.body.mobile + '&text=' + encodeURIComponent(req.body.message);

  request({
    url: url + params,
    method: "GET",
  }, function (error, response, body) {
    res.json(body);
    console.log(body);
  });

});
router.post('/message/retailer/sms/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/retailers/sms/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });

});
router.post('/message/retailer/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/retailers/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });

});
router.get('/get/message/fieldforce/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/fieldforces/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.get('/get/message/retailer/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/retailers/",
      method: "GET",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });
});
router.post('/message/fieldforce/sms/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/fieldforces/sms/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });

});
router.post('/message/fieldforce/', (req, res, next) => {
  let creds = {
    password: "icanseeyou",
    username: "pvsune"
  };
  request({
    url: host + "/auth/token/login/",
    method: "POST",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
    body: creds
  }, function (error, response, body) {
    let data = req.body;
    let token = 'Token ' + body.auth_token;
    console.log(data);
    request({
      url: host + "/messages/fieldforces/",
      method: "POST",
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: data
    }, function (error, response, body) {
      res.json(body);
      console.log(body);
    });
  });

});
router.get('/region/', (req, res, next) => {
  request({
    url: host + "/static/locations.json",
    method: "GET",
    json: true,
    headers: {
      'Content-Type': 'application/json'
    },
  }, function (error, response, body) {
    res.json(body);
    // console.log(body);
  });
});

module.exports = router;
